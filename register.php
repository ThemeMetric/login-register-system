<?php
session_start();
require_once('class.user.php');
$user = new USER();
if($user->is_loggedin()!=""){
	$user->redirect('admin.php');
}

if(isset($_POST['btn-signup'])){
	
	$umail = strip_tags($_POST['txt_umail']);
	$upass = strip_tags($_POST['txt_upass']);	
	
	if($umail==""){
		$error[] = "provide email id !";
	}
	
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL)){
	    $error[] = 'Please enter a valid email address !';
	}
	else if($upass==""){
		$error[] = "provide password !";
	}
	else if(strlen($upass) < 6){
		$error[] = "Password must be atleast 6 characters";	
	}
	else
	{
		try{
	$stmt = $user->runQuery("SELECT  user_email FROM users WHERE  user_email=:umail");
			$stmt->execute(array(':umail'=>$umail));
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
				
			
			 if($row['user_email']==$umail){
				$error[] = "sorry email id already taken !";
			}
			else{
				if($user->register($umail,$upass)){	
					$user->redirect('register.php?joined');
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}	
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Register</title>
<link href="assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="shortcut icon" href="assets/admin/img/favicons/favicon.ico">

<body id="app" class="login-page">
<div class="login-wrapper">
    <div class="login-box">
                <div class="brand-main">
            <a href="#"><img src="assets/admin/img/logo-large.png" alt="Laraspace Logo"></a>
        </div>
        <form action="" id="registerForm" method="POST" enctype='multipart/form-data'>
       <!-- <input type="hidden" name="_token" value="<?php echo $code?>"> -->
            
            <?php
		if(isset($error)){
		   foreach($error as $error)
		{
		?>
                     <div class="alert alert-danger">
                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
                     </div>
                     <?php
				}
			}
			else if(isset($_GET['joined']))
			{
				 ?>
            
                 <div class="alert alert-info">
                      <i class="glyphicon glyphicon-log-in"></i> &nbsp; Successfully registered <a href='index.php'>login</a> here
                 </div>
                 <?php
			}
		 ?>  
            
            
        <div class="form-group">
            <input type="email" class="form-control form-control-danger" placeholder="Enter email" name="txt_umail" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control form-control-danger" placeholder="Enter Password" name="txt_upass" id="password" required="required">
        </div>
        
        <div class="form-group">
            <input type="password" class="form-control form-control-danger" placeholder="Retype Password" name="password_confirmation" >
        </div>
        <button type="submit" class="btn btn-login btn-full" name="btn-signup">Register</button>
    </form>
          <div class="page-copyright">
           
              <a class="btn btn-login btn-full" href="index.php">Login</a>
           
        </div>
        <div class="page-copyright">
            <p>If you have account Login</p>
            
        </div>
    </div>
</div>
<script src="assets/admin/js/core/plugins.js"></script>
<script src="assets/admin/js/sessions/register.js"></script>
</body>
</html>
