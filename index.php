<?php
session_start();
require_once("class.user.php");
$login = new USER();

if($login->is_loggedin()!=""){
	$login->redirect('admin.php');
}

if(isset($_POST['btn-login'])){	
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);		
	if($login->doLogin($umail,$upass)){
		$login->redirect('admin.php');
	}
	else{
		$error = "Wrong Details !";
	}	
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<link href="assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="assets/admin/img/favicons/favicon.ico">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="assets/admin/img/favicons/mstile-144x144.png">
<meta name="msapplication-config" content="assets/admin/img/favicons/browserconfig.xml">

<body id="app" class="login-page">
<div class="login-wrapper">
    <div class="login-box">
                <div class="brand-main">
            <a href="#"><img src="assets/admin/img/logo-large.png" alt="Laraspace Logo"></a>
        </div>
        
     <div id="error">
        <?php
	if(isset($error)){
	?>
      <div class="alert alert-danger">
        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
      </div>
       <?php
	}
        ?>
        </div>
        
        
    <form action="" id="loginForm" method="post">
              
    <input type="hidden" name="_token" value="rPQsDEzdgkRUwlxjHFTpiIIyqrAmhDyVXbvan2Ee">
    <div class="form-group">
      <input type="email" class="form-control form-control-danger" placeholder="Enter email" name="txt_uname_email">
    </div>
    <div class="form-group">
      <input type="password" class="form-control form-control-danger" placeholder="Enter Password" name="txt_password">
    </div>
    <div class="other-actions row">
      <div class="col-sm-6">
        <div class="checkbox">
          <label class="c-input c-checkbox">
            <input type="checkbox" name="remember">
            <span class="c-indicator"></span>
            Remember Me
          </label>
        </div>
      </div>
      <div class="col-sm-6 text-sm-right">
        <a href="#" class="forgot-link">Forgot Password?</a>
      </div>
    </div>
    <button type="submit" class="btn btn-login btn-full" name="btn-login">Login</button>
  </form>
        <div class="page-copyright">
           
           <a class="btn btn-login btn-full" href="register.php">Register</a>
           
        </div>
        <div class="page-copyright">
            <p>If you haven't account register</p>
           
        </div>
    </div>
</div>
<script src="assets/admin/js/core/plugins.js"></script>
  <script src="assets/admin/js/sessions/login.js"></script>
</body>
</html>
