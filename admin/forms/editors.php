<!DOCTYPE html>
<html>
<head>
<title>Editor</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
<script src="../../assets/admin/js/core/pace.js"></script>
<link href="../../assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="../../assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../../assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../../assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../../assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../../assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../../assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../../assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../../assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../../assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="../../assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="../../assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="../../assets/admin/img/favicons/favicon.ico">
</head>
<body id="app" class="sidebar-default">
        <header class="site-header">
  <a href="#" class="brand-main">
    <img src="../../assets/admin/img/logo-desk.png" alt="Laraspace Logo" class="hidden-sm-down">
    <img src="../../assets/admin/img/logo-mobile.png" alt="Laraspace Logo" class="hidden-md-up">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>

    <ul class="action-list">
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#">New Post</a>
          <a class="dropdown-item" href="#">New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="../../assets/admin/img/avatar.png" alt="Avatar"></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-cogs"></i> Settings</a>
          <a class="dropdown-item" href="../../logout.php?logout=true"><i class="fa fa-sign-out"></i> Logout</a>
        </div>
      </li>
    </ul>
</header>
    <div class="mobile-menu-overlay"></div>
    <div class="sidebar-left " id="mobile-nav">
    <div class="sidebar-body scroll-pane">
        <ul class="side-nav">
    <li class="  has-child">
        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../../admin.php">Basic</a></li>
            <li class=""><a href="../icon-sidebar.php">Icon Sidebar</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-star"></i> Basic UI
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../basic-ui/buttons.php">Buttons</a></li>
            <li class=""><a href="../basic-ui/cards.php">Cards</a></li>
            <li class=""><a href="../basic-ui/tabs.php">Tabs & Accordians</a></li>
            <li class=""><a href="../basic-ui/typography.php">Typography</a></li>
            <li class=""><a href="../basic-ui/tables.php">Tables</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-puzzle-piece"></i> Components
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../components/datatables.php">Datatables</a></li>
            <li class=""><a href="../components/notifications.php">Notifications</a></li>
            <li class=""><a href="../components/graphs.php">Graphs</a></li>
        </ul>
    </li>
    <li class="has-child active open">
        <a href="#">
            <i class="fa fa-rocket"></i> Forms
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="general.php">General Elements</a></li>
            <li class=""><a href="advanced.php">Advanced Elements</a></li>
            <li class=""><a href="layouts.php">Form Layouts</a></li>
            <li class=""><a href="validation.php">Form Validation</a></li>
            <li class="active"><a href="editors.php">Editors</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-file"></i> Pages
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a target="_blank" href="../../index.php">Login</a></li>
            <li class=""><a target="_blank" href="../../register.php">Register</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-user"></i> Users
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../users.php">All Users</a></li>
            <li class=""><a href="../users/1.php">User Profile</a></li>
        </ul>
    </li>
    <li class=""><a href="../todos.php"><i class="fa fa-check"></i> Todo App</a></li>
    <li class=""><a href="../settings.php"><i class="fa fa-cogs"></i> Settings</a></li>
</ul>    </div>
</div>

        <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Editors</h3>
            <ol class="breadcrumb">
                <li><a href="../../admin.php">Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Summernote</h3>
            </div>
            <div class="card-block">
                <div class="ls-summernote">
                    Hello Summernote
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Simple MDE</h3>
            </div>
            <div class="card-block">
                <textarea class="ls-simplemde">
# Intro
Go ahead, play around with the editor! Be sure to check out **bold** and *italic* styling, or even [links](http://google.com). You can type the Markdown syntax, use the toolbar, or use shortcuts like `cmd-b` or `ctrl-b`.

## Lists
Unordered lists can be started using the toolbar or by typing `* `, `- `, or `+ `. Ordered lists can be started by typing `1. `.

#### Unordered
* Lists are a piece of cake
* They even auto continue as you type
* A double enter will end them
* Tabs and shift-tabs work too

#### Ordered
1. Numbered lists...
2. ...work too!

## What about images?
![Yes](http://i.imgur.com/sZlktY7.png)
                </textarea>
            </div>
        </div>
        </div>

    <footer class="site-footer">
    <div class="text-right">
     Login Register system
    </div>
</footer>
    <script src="../../assets/admin/js/core/plugins.js"></script>
    </body>
</html>
