<!DOCTYPE html>
<html>
<head>
<title>Forms</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
<script src="../../assets/admin/js/core/pace.js"></script>
<link href="../../assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="../../assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../../assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../../assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../../assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../../assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../../assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../../assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../../assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../../assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="../../assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="../../assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="../../assets/admin/img/favicons/favicon.ico">
</head>
<body id="app" class="sidebar-default">
        <header class="site-header">
  <a href="#" class="brand-main">
    <img src="../../assets/admin/img/logo-desk.png" alt="Laraspace Logo" class="hidden-sm-down">
    <img src="../../assets/admin/img/logo-mobile.png" alt="Laraspace Logo" class="hidden-md-up">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>

    <ul class="action-list">
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#">New Post</a>
          <a class="dropdown-item" href="#">New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="../../assets/admin/img/avatar.png" alt="Avatar"></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-cogs"></i> Settings</a>
          <a class="dropdown-item" href="../../logout.php?logout=true"><i class="fa fa-sign-out"></i> Logout</a>
        </div>
      </li>
    </ul>
</header>
    <div class="mobile-menu-overlay"></div>
    <div class="sidebar-left " id="mobile-nav">
    <div class="sidebar-body scroll-pane">
        <ul class="side-nav">
    <li class="  has-child">
        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../../admin.php">Basic</a></li>
            <li class=""><a href="../icon-sidebar.php">Icon Sidebar</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-star"></i> Basic UI
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../basic-ui/buttons.php">Buttons</a></li>
            <li class=""><a href="../basic-ui/cards.php">Cards</a></li>
            <li class=""><a href="../basic-ui/tabs.php">Tabs & Accordians</a></li>
            <li class=""><a href="../basic-ui/typography.php">Typography</a></li>
            <li class=""><a href="../basic-ui/tables.php">Tables</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-puzzle-piece"></i> Components
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../components/datatables.php">Datatables</a></li>
            <li class=""><a href="../components/notifications.php">Notifications</a></li>
            <li class=""><a href="../components/graphs.php">Graphs</a></li>
        </ul>
    </li>
    <li class="has-child active open">
        <a href="#">
            <i class="fa fa-rocket"></i> Forms
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="general.php">General Elements</a></li>
            <li class="active"><a href="advanced.php">Advanced Elements</a></li>
            <li class=""><a href="layouts.php">Form Layouts</a></li>
            <li class=""><a href="validation.php">Form Validation</a></li>
            <li class=""><a href="editors.php">Editors</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-file"></i> Pages
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a target="_blank" href="../../index.php">Login</a></li>
            <li class=""><a target="_blank" href="../../register.php">Register</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-user"></i> Users
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../users.php">All Users</a></li>
            <li class=""><a href="../users/1.php">User Profile</a></li>
        </ul>
    </li>
    <li class=""><a href="../todos.php"><i class="fa fa-check"></i> Todo App</a></li>
    <li class=""><a href="../settings.php"><i class="fa fa-cogs"></i> Settings</a></li>
</ul>    </div>
</div>

        <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Advanced Elements</h3>
            <ol class="breadcrumb">
                <li><a href="../../admin.php">Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Advanced Elements</li>
            </ol>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Select 2 <a class="source-link" href="https://select2.github.io/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-4 col-md-6 m-b-2">
                        <h5 class="section-semi-title">
                            Single Select
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-select2</code> class to a select element.</p>
                        <select class="form-control ls-select2">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                        </select>
                    </div>
                    <div class="col-xl-4 col-md-6 m-b-2">
                        <h5 class="section-semi-title">
                            with Groups
                        </h5>
                        <p class="m-b-2">Just use <code>optgroups</code> with label attribute inside select element.</p>
                        <select class="form-control ls-select2">
                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                            </optgroup>
                            <optgroup label="Pacific Time Zone">
                                <option value="CA">California</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </optgroup>
                            <optgroup label="Mountain Time Zone">
                                <option value="AZ">Arizona</option>
                                <option value="CO">Colorado</option>
                                <option value="ID">Idaho</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NM">New Mexico</option>
                                <option value="ND">North Dakota</option>
                                <option value="UT">Utah</option>
                                <option value="WY">Wyoming</option>
                            </optgroup>
                            <optgroup label="Central Time Zone">
                                <option value="AL">Alabama</option>
                                <option value="AR">Arkansas</option>
                                <option value="IL">Illinois</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="OK">Oklahoma</option>
                                <option value="SD">South Dakota</option>
                                <option value="TX">Texas</option>
                                <option value="TN">Tennessee</option>
                                <option value="WI">Wisconsin</option>
                            </optgroup>
                            <optgroup label="Eastern Time Zone">
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="IN">Indiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="OH">Ohio</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WV">West Virginia</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-xl-4 col-md-6 m-b-2">
                        <h5 class="section-semi-title">
                            Multiple Select
                        </h5>
                        <p class="m-b-2">Just use <code>multiple</code> attribute inside select element.</p>
                        <select class="form-control ls-select2" multiple="multiple">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Bootstrap Select <a class="source-link" href="http://silviomoreto.github.io/bootstrap-select/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            Single Select
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-bootstrap-select</code> class to a select element</p>
                        <select class="form-control ls-bootstrap-select">
                            <option>Mustard</option>
                            <option>Ketchup</option>
                            <option>Relish</option>
                        </select>

                    </div>
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            with Groups
                        </h5>
                        <p class="m-b-2">Just use <code>optgroups</code> with label attribute inside select element.</p>
                        <select class="form-control ls-bootstrap-select">
                            <optgroup label="Picnic">
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </optgroup>
                            <optgroup label="Camping">
                                <option>Tent</option>
                                <option>Flashlight</option>
                                <option>Toilet Paper</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            Multiple Select
                        </h5>
                        <p class="m-b-2">Just use <code>multiple</code> attribute inside select element.</p>
                        <select class="form-control ls-bootstrap-select" multiple>
                            <option>Mustard</option>
                            <option>Ketchup</option>
                            <option>Relish</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            Live Search
                        </h5>
                        <p class="m-b-2">Just use <code>data-live-search="true"</code> attribute inside select element to enable live search.</p>
                        <select class="form-control ls-bootstrap-select" data-live-search="true">
                            <option>Hot Dog, Fries and a Soda</option>
                            <option>Burger, Shake and a Smile</option>
                            <option>Sugar, Spice and all things nice</option>
                        </select>
                    </div>
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            Keywords
                        </h5>
                        <p class="m-b-2">Add key words to options to improve their searchability using <code>data-tokens</code>.</p>
                        <select class="form-control ls-bootstrap-select" data-live-search="true">
                            <option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
                            <option data-tokens="mustard">Burger, Shake and a Smile</option>
                            <option data-tokens="frosting">Sugar, Spice and all things nice</option>
                        </select>
                    </div>
                    <div class="col-md-4 m-b-3">
                        <h5 class="section-semi-title">
                            Limit Selected Items
                        </h5>
                        <p class="m-b-2">Limit the number of options that can be selected via the <code>data-max-options</code> attribute.</p>
                        <select class="form-control ls-bootstrap-select" multiple="multiple" data-max-options="2">
                            <option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
                            <option data-tokens="mustard">Burger, Shake and a Smile</option>
                            <option data-tokens="frosting">Sugar, Spice and all things nice</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Multiple Select <a class="source-link" href="http://loudev.com/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            Default
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-multi-select</code> class to a select element to make it a multi-select</p>
                        <select class="form-control ls-multi-select" multiple="multiple">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                        </select>
                    </div>
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            Pre-Selected Options
                        </h5>
                        <p class="m-b-2">Just use <code>selected</code> attribute on the options which you want to keep as selected</p>
                        <select class="form-control ls-multi-select" multiple="multiple">
                            <option value="AK" selected>Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR" selected>Oregon</option>
                            <option value="WA" selected>Washington</option>
                        </select>
                    </div>
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            with Groups
                        </h5>
                        <p class="m-b-2">Just use <code>optgroups</code> with label attribute inside select element</p>
                        <select class="form-control ls-multi-select" multiple="multiple">
                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                            </optgroup>
                            <optgroup label="Pacific Time Zone">
                                <option value="CA">California</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </optgroup>
                            <optgroup label="Mountain Time Zone">
                                <option value="AZ">Arizona</option>
                                <option value="CO">Colorado</option>
                                <option value="ID">Idaho</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NM">New Mexico</option>
                                <option value="ND">North Dakota</option>
                                <option value="UT">Utah</option>
                                <option value="WY">Wyoming</option>
                            </optgroup>
                            <optgroup label="Central Time Zone">
                                <option value="AL">Alabama</option>
                                <option value="AR">Arkansas</option>
                                <option value="IL">Illinois</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="OK">Oklahoma</option>
                                <option value="SD">South Dakota</option>
                                <option value="TX">Texas</option>
                                <option value="TN">Tennessee</option>
                                <option value="WI">Wisconsin</option>
                            </optgroup>
                            <optgroup label="Eastern Time Zone">
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="IN">Indiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="OH">Ohio</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WV">West Virginia</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Switch Toggles <a class="source-link" href="http://abpetkov.github.io/switchery/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-lg-4 m-b-2">
                        <h5 class="section-semi-title">
                            Basic Switch
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-switch</code> class to a checkbox to make it a switch</p>
                        <input type="checkbox" class="ls-switch" checked />
                    </div>
                    <div class="col-lg-4 m-b-2">
                        <h5 class="section-semi-title">
                            Sizes
                        </h5>
                        <p class="m-b-2">use <code>data-size</code> attribute to change the size of the switch</p>
                        <input type="checkbox" class="ls-switch" data-size="small" checked />
                        <input type="checkbox" class="ls-switch" checked />
                        <input type="checkbox" class="ls-switch" data-size="large" checked />
                    </div>
                    <div class="col-lg-4 m-b-2">
                        <h5 class="section-semi-title">
                            Colors
                        </h5>
                        <p class="m-b-2">use <code>data-color="#007dcc"</code> to change the color of the switch</p>
                        <input type="checkbox" class="ls-switch" checked data-color="#007dcc"/>
                        <input type="checkbox" class="ls-switch" checked data-color="#4fc47f" />
                        <input type="checkbox" class="ls-switch" checked data-color="#f35a3d"/>
                        <input type="checkbox" class="ls-switch" checked  data-color="#f0ad4e"/>
                        <input type="checkbox" class="ls-switch" checked data-color="#5BBFDE" />

                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Clock Time Picker <a class="source-link" href="http://weareoutman.github.io/clockpicker/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-4  col-md-6 m-b-2">
                        <h5 class="section-semi-title">
                            Default
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-clockpicker</code> class to an input to make it a clockpicker</p>
                        <div class="input-group ls-clockpicker" data-autoclose="false">
                            <input type="text" class="form-control" value="09:30">
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 m-b-2">
                        <h5 class="section-semi-title">
                            Autoclose and align top left
                        </h5>
                        <p class="m-b-2">use <code>data-autoclose="true"</code> attribute to auto-close clockpicker after the date is selected and <code>data-placement="left"</code>,<code>data-align="top"</code> for placement of the clockpicker</p>
                        <div class="input-group ls-clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                            <input type="text" class="form-control" value="09:30">
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Date Picker <a class="source-link" href="https://github.com/eternicode/bootstrap-datepicker" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 m-b-2">
                        <h5 class="section-semi-title">
                            Default
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-datepicker</code> class to a input.</p>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" class="form-control ls-datepicker" value="09:30" >
                         </div>
                    </div>

                    <div class="col-xl-4 col-lg-6 m-b-2">
                        <h5 class="section-semi-title">
                            Date Range
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-datepicker</code> classes to both inputs.</p>
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control ls-datepicker">
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control ls-datepicker">
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-6 m-b-2">
                        <h5 class="section-semi-title">
                            Inline
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-datepicker</code> classes to a div element.</p>
                        <div data-date="12/03/2012" class="ls-datepicker"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Time Picker <a class="source-link" href="http://jonthornton.github.io/jquery-timepicker/" target="_blank">source</a></h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            Default
                        </h5>
                        <p class="m-b-2">Just apply <code>.ls-timepicker</code> class to an input.</p>
                        <div class="input-group">
                            <input type="text" class="form-control ls-timepicker" value="09:30">
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            Show Duration
                        </h5>
                        <p class="m-b-2">Use <code>data-duration="true"</code> attribute to show the time duration.</p>
                        <div class="input-group">
                            <input type="text" class="form-control ls-timepicker" value="09:30" data-duration="true">
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-4 m-b-2">
                        <h5 class="section-semi-title">
                            Time Format
                        </h5>
                        <p class="m-b-2">Use <code>data-format="H:i:s"</code> attribute to change the time format.</p>
                        <div class="input-group">
                            <input type="text" class="form-control ls-timepicker" value="09:30" data-format="H:i:s">
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <footer class="site-footer">
    <div class="text-right">
      Login Register system
    </div>
</footer>
    <script src="../../assets/admin/js/core/plugins.js"></script>
    </body>
</html>
