<!DOCTYPE html>
<html>
<head>
<title>Tabs</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
<script src="../../assets/admin/js/core/pace.js"></script>
<link href="../../assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="../../assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../../assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../../assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../../assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../../assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../../assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../../assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../../assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../../assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="../../assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="../../assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="../../assets/admin/img/favicons/favicon.ico">
</head>
<body id="app" class="sidebar-default">
        <header class="site-header">
  <a href="#" class="brand-main">
    <img src="../../assets/admin/img/logo-desk.png" alt="Laraspace Logo" class="hidden-sm-down">
    <img src="../../assets/admin/img/logo-mobile.png" alt="Laraspace Logo" class="hidden-md-up">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>

    <ul class="action-list">
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#">New Post</a>
          <a class="dropdown-item" href="#">New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="../../assets/admin/img/avatar.png" alt="Avatar"></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-cogs"></i> Settings</a>
          <a class="dropdown-item" href="../../logout.php?logout=true"><i class="fa fa-sign-out"></i> Logout</a>
        </div>
      </li>
    </ul>
</header>
    <div class="mobile-menu-overlay"></div>
    <div class="sidebar-left " id="mobile-nav">
    <div class="sidebar-body scroll-pane">
        <ul class="side-nav">
    <li class="  has-child">
        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../../admin.php">Basic</a></li>
            <li class=""><a href="../icon-sidebar.php">Icon Sidebar</a></li>
        </ul>
    </li>
    <li class="has-child active open">
        <a href="#">
            <i class="fa fa-star"></i> Basic UI
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="buttons.php">Buttons</a></li>
            <li class=""><a href="cards.php">Cards</a></li>
            <li class="active"><a href="tabs.php">Tabs & Accordians</a></li>
            <li class=""><a href="typography.php">Typography</a></li>
            <li class=""><a href="tables.php">Tables</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-puzzle-piece"></i> Components
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../components/datatables.php">Datatables</a></li>
            <li class=""><a href="../components/notifications.php">Notifications</a></li>
            <li class=""><a href="../components/graphs.php">Graphs</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-rocket"></i> Forms
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../forms/general.php">General Elements</a></li>
            <li class=""><a href="../forms/advanced.php">Advanced Elements</a></li>
            <li class=""><a href="../forms/layouts.php">Form Layouts</a></li>
            <li class=""><a href="../forms/validation.php">Form Validation</a></li>
            <li class=""><a href="../forms/editors.php">Editors</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-file"></i> Pages
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a target="_blank" href="../../index.php">Login</a></li>
            <li class=""><a target="_blank" href="../../register.php">Register</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-user"></i> Users
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../users.php">All Users</a></li>
            <li class=""><a href="../users/1.php">User Profile</a></li>
        </ul>
    </li>
    <li class=""><a href="../todos.php"><i class="fa fa-check"></i> Todo App</a></li>
    <li class=""><a href="../settings.php"><i class="fa fa-cogs"></i> Settings</a></li>
</ul>    </div>
</div>

        <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Tabs & Accordians</h3>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">UI Elements</a></li>
                <li class="active">Tabs & Accordians</li>
            </ol>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>Tabs</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-lg-6 m-b-2">
                        <h5 class="section-semi-title">Tabs Default</h5>
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos nesciunt praesentium totam vitae? Consequatur delectus eos esse natus odio. Expedita?
                                </div>
                                <div class="tab-pane" id="profile" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus nulla quia quod sed voluptatem. Aliquam atque error illum nihil quae.
                                </div>
                                <div class="tab-pane" id="messages" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus accusantium aliquam delectus doloribus eius maxime nisi quia rerum soluta.
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam amet ea, excepturi fuga fugiat magnam non ratione sunt tenetur.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 m-b-2">
                        <h5 class="section-semi-title">Tabs Primary</h5>
                        <div class="tabs tabs-primary">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages2" role="tab">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">Settings</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos nesciunt praesentium totam vitae? Consequatur delectus eos esse natus odio. Expedita?
                                </div>
                                <div class="tab-pane" id="profile2" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus nulla quia quod sed voluptatem. Aliquam atque error illum nihil quae.
                                </div>
                                <div class="tab-pane" id="messages2" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus accusantium aliquam delectus doloribus eius maxime nisi quia rerum soluta.
                                </div>
                                <div class="tab-pane" id="settings2" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam amet ea, excepturi fuga fugiat magnam non ratione sunt tenetur.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 m-b-2">
                        <h5 class="section-semi-title">Tabs Vertical</h5>
                        <div class="tabs tabs-vertical">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#settings1" role="tab">Settings</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home1" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos nesciunt praesentium totam vitae? Consequatur delectus eos esse natus odio. Expedita?
                                </div>
                                <div class="tab-pane" id="profile1" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus nulla quia quod sed voluptatem. Aliquam atque error illum nihil quae.
                                </div>
                                <div class="tab-pane" id="messages1" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus accusantium aliquam delectus doloribus eius maxime nisi quia rerum soluta.
                                </div>
                                <div class="tab-pane" id="settings1" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam amet ea, excepturi fuga fugiat magnam non ratione sunt tenetur.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 m-b-2">
                        <h5 class="section-semi-title">Tabs Vertical Primary</h5>
                        <div class="tabs tabs-primary tabs-vertical">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#settings3" role="tab">Settings</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home3" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos nesciunt praesentium totam vitae? Consequatur delectus eos esse natus odio. Expedita?
                                </div>
                                <div class="tab-pane" id="profile3" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus nulla quia quod sed voluptatem. Aliquam atque error illum nihil quae.
                                </div>
                                <div class="tab-pane" id="messages3" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus accusantium aliquam delectus doloribus eius maxime nisi quia rerum soluta.
                                </div>
                                <div class="tab-pane" id="settings3" role="tabpanel">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam amet ea, excepturi fuga fugiat magnam non ratione sunt tenetur.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4>Accordians</h4>
            </div>
            <div class="col-lg-6 m-t-2">
                <h5 class="section-semi-title">Accordian Default</h5>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Collapsible Group Item #1
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse panel-content in" role="tabpanel" aria-labelledby="headingOne">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <a class="collapsed panel-title"  data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Collapsible Group Item #2
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse panel-content" role="tabpanel" aria-labelledby="headingTwo">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <a class="collapsed panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Collapsible Group Item #3
                            </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse panel-content" role="tabpanel" aria-labelledby="headingThree">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="site-footer">
    <div class="text-right">
      Login Register system
    </div>
</footer>
    <script src="../../assets/admin/js/core/plugins.js"></script>
    </body>
</html>
