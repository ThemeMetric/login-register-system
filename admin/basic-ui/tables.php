<!DOCTYPE html>
<html>

<head>
<title>Tables</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
<script src="../../assets/admin/js/core/pace.js"></script>
<link href="../../assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="../../assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../../assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../../assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../../assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../../assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../../assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../../assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../../assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../../assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="../../assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="../../assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="../../assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="../../assets/admin/img/favicons/favicon.ico">
</head>
<body id="app" class="sidebar-default">
        <header class="site-header">
  <a href="#" class="brand-main">
    <img src="../../assets/admin/img/logo-desk.png" alt="Laraspace Logo" class="hidden-sm-down">
    <img src="../../assets/admin/img/logo-mobile.png" alt="Laraspace Logo" class="hidden-md-up">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>

    <ul class="action-list">
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#">New Post</a>
          <a class="dropdown-item" href="#">New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="../../assets/admin/img/avatar.png" alt="Avatar"></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-cogs"></i> Settings</a>
          <a class="dropdown-item" href="../../logout.php?logout=true"><i class="fa fa-sign-out"></i> Logout</a>
        </div>
      </li>
    </ul>
</header>
    <div class="mobile-menu-overlay"></div>
    <div class="sidebar-left " id="mobile-nav">
    <div class="sidebar-body scroll-pane">
        <ul class="side-nav">
    <li class="  has-child">
        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../../admin.php">Basic</a></li>
            <li class=""><a href="../icon-sidebar.php">Icon Sidebar</a></li>
        </ul>
    </li>
    <li class="has-child active open">
        <a href="#">
            <i class="fa fa-star"></i> Basic UI
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="buttons.php">Buttons</a></li>
            <li class=""><a href="cards.php">Cards</a></li>
            <li class=""><a href="tabs.php">Tabs & Accordians</a></li>
            <li class=""><a href="typography.php">Typography</a></li>
            <li class="active"><a href="tables.php">Tables</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-puzzle-piece"></i> Components
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../components/datatables.php">Datatables</a></li>
            <li class=""><a href="../components/notifications.php">Notifications</a></li>
            <li class=""><a href="../components/graphs.php">Graphs</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-rocket"></i> Forms
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../forms/general.php">General Elements</a></li>
            <li class=""><a href="../forms/advanced.php">Advanced Elements</a></li>
            <li class=""><a href="../forms/layouts.php">Form Layouts</a></li>
            <li class=""><a href="../forms/validation.php">Form Validation</a></li>
            <li class=""><a href="../forms/editors.php">Editors</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-file"></i> Pages
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a target="_blank" href="../../indrx.php">Login</a></li>
            <li class=""><a target="_blank" href="../../register.php">Register</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-user"></i> Users
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="../users.php">All Users</a></li>
            <li class=""><a href="../users/1.php">User Profile</a></li>
        </ul>
    </li>
    <li class=""><a href="../todos.php"><i class="fa fa-check"></i> Todo App</a></li>
    <li class=""><a href="../settings.php"><i class="fa fa-cogs"></i> Settings</a></li>
</ul>    </div>
</div>

        <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Tables</h3>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Basic UI</a></li>
                <li class="active">Tables</li>
            </ol>
        </div>
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Default Table</h5>
                        <p>This is just a Simple Bootstrap style table. To use this just apply <mark>.table</mark> class to your table element.</p>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td>$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Bordered Table</h5>
                        <p>To use this just apply <mark>.table</mark>, <mark>.table-bordered</mark> classes to your table element to make it bordered.</p>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td>$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Hover Table</h5>
                        <p>To use this just apply <mark>.table</mark>, <mark>.table-hover</mark> classes to your table element to make it hoverable.</p>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td>$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Striped Table</h5>
                        <p>To use this just apply <mark>.table</mark>, <mark>.table-bordered</mark> classes to your table element to make it striped.</p>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td>$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Small Table</h5>
                        <p>To use this just apply <mark>.table</mark>, <mark>.table-sm</mark> class to your table element to make it small.</p>
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td>$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 m-b-2">
                        <h5 class="section-semi-title">Contextual Classes</h5>
                        <p>To use this just apply <mark>.table-active</mark>, <mark>.table-success</mark>, <mark>.table-warning</mark>, <mark>.table-danger</mark>, <mark>.table-info</mark> classes on the row or cells.</p>
                        <table class="table table-default">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>MRP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="table-success">
                                <td>1</td>
                                <td>Colgate Toothpaste</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$5</td>
                                <td>$6</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Pink T-Shirt</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$20</td>
                                <td>$40</td>
                            </tr>
                            <tr class="table-danger">
                                <td>3</td>
                                <td>Nike Running Shoes</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr class="table-warning">
                                <td>4</td>
                                <td>Reebok Casual Shoes</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$70</td>
                                <td>$80</td>
                            </tr>
                            <tr>
                                <td class="table-success">5</td>
                                <td>Titan Raga Premium Watch</td>
                                <td> <span class="label label-success">in stock</span></td>
                                <td>$100</td>
                                <td>$120</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Philips Hair Remover</td>
                                <td> <span class="label label-danger">out of stock</span></td>
                                <td>$50</td>
                                <td class="table-danger">$70</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <footer class="site-footer">
    <div class="text-right">
     Login Register system
    </div>
</footer>
    <script src="../../assets/admin/js/core/plugins.js"></script>
    </body>

</html>
