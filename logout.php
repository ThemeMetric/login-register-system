<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('session.php');
require_once('class.user.php');
$user_logout = new USER();

if($user_logout->is_loggedin()!=""){
        $user_logout->redirect('admin.php');
}
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
        $user_logout->doLogout();
        $user_logout->redirect('index.php');
}
