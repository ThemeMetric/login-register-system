<?php
require_once("session.php");
require_once("class.user.php");
$auth_user = new USER();
$user_id = $_SESSION['user_session'];
$stmt = $auth_user->runQuery("SELECT * FROM users WHERE user_id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));
$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
<script src="assets/admin/js/core/pace.js"></script>
<link href="assets/admin/css/laraspace.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="assets/admin/img/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/admin/img/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/admin/img/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/admin/img/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/admin/img/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/admin/img/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/admin/img/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/admin/img/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/admin/img/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="assets/admin/img/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="assets/admin/img/favicons/manifest.json">
<link rel="mask-icon" href="assets/admin/img/favicons/safari-pinned-tab.svg" color="#333333">
<link rel="shortcut icon" href="assets/admin/img/favicons/favicon.ico">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/assets/admin/img/favicons/mstile-144x144.png">
<meta name="msapplication-config" content="/assets/admin/img/favicons/browserconfig.xml">
</head>
<body id="app" class="sidebar-default">
 <header class="site-header">
    <a href="admin.php" class="brand-main">
      <img src="assets/admin/img/logo-desk.png" alt="Laraspace Logo" class="hidden-sm-down">
      <img src="assets/admin/img/logo-mobile.png" alt="Laraspace Logo" class="hidden-md-up">
    </a>
    <a href="#" class="nav-toggle">
      <div class="hamburger hamburger--htla">
        <span>toggle menu</span>
      </div>
    </a>

    <ul class="action-list">
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#">New Post</a>
          <a class="dropdown-item" href="#">New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="assets/admin/img/avatar.png" alt="Avatar"></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-cogs"></i> Settings</a>
          <a class="dropdown-item" href="logout.php?logout=true"><i class="fa fa-sign-out"></i>Logout</a>
        </div>
      </li>
    </ul>
</header>
    <div class="mobile-menu-overlay"></div>
    <div class="sidebar-left " id="mobile-nav">
    <div class="sidebar-body scroll-pane">
        <ul class="side-nav">
    <li class="active open  has-child">
        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        <ul class="sub-menu collapse">
            <li class="active"><a href="admin.php">Basic</a></li>
            <li class=""><a href="admin/icon-sidebar.php">Icon Sidebar</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-star"></i> Basic UI
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="admin/basic-ui/buttons.php">Buttons</a></li>
            <li class=""><a href="admin/basic-ui/cards.php">Cards</a></li>
            <li class=""><a href="admin/basic-ui/tabs.php">Tabs & Accordians</a></li>
            <li class=""><a href="admin/basic-ui/typography.php">Typography</a></li>
            <li class=""><a href="admin/basic-ui/tables.php">Tables</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-puzzle-piece"></i> Components
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="admin/components/datatables.php">Datatables</a></li>
            <li class=""><a href="admin/components/notifications.php">Notifications</a></li>
            <li class=""><a href="admin/components/graphs.php">Graphs</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-rocket"></i> Forms
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="admin/forms/general.php">General Elements</a></li>
            <li class=""><a href="admin/forms/advanced.php">Advanced Elements</a></li>
            <li class=""><a href="admin/forms/layouts.php">Form Layouts</a></li>
            <li class=""><a href="admin/forms/validation.php">Form Validation</a></li>
            <li class=""><a href="admin/forms/editors.php">Editors</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-file"></i> Pages
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a target="_blank" href="index.php">Login</a></li>
            <li class=""><a target="_blank" href="register.php">Register</a></li>
        </ul>
    </li>
    <li class="has-child ">
        <a href="#">
            <i class="fa fa-user"></i> Users
        </a>
        <ul class="sub-menu collapse">
            <li class=""><a href="admin/users.php">All Users</a></li>
            <li class=""><a href="admin/users/1.php">User Profile</a></li>
        </ul>
    </li>
    <li class=""><a href="admin/todos.php"><i class="fa fa-check"></i> Todo App</a></li>
    <li class=""><a href="admin/settings.php"><i class="fa fa-cogs"></i> Settings</a></li>
</ul>    </div>
</div>

    <div class="main-content">
  <div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
      <a class="dashbox" href="#">
        <i class="fa fa-envelope text-primary"></i>
        <span class="title">
          35
        </span>
        <span class="desc">
          Mails
        </span>
      </a>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
      <a class="dashbox" href="#">
        <i class="fa fa-ticket text-success"></i>
        <span class="title">
          200
        </span>
        <span class="desc">
          Pending Tickets
        </span>
      </a>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
      <a class="dashbox" href="#">
        <i class="fa fa-shopping-cart text-danger"></i>
        <span class="title">
          100
        </span>
        <span class="desc">
          New Orders
        </span>
      </a>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
      <a class="dashbox" href="#">
        <i class="fa fa-comments text-info"></i>
        <span class="title">
          59
        </span>
        <span class="desc">
          Comments
        </span>
      </a>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xl-6 m-t-2">
      <div class="card">
        <div class="card-header">
          <h4><i class="fa fa-line-chart text-primary"></i> Monthly Sales</h4>
        </div>
        <div class="card-block">
          <line-graph :labels="['Jan','Feb','Mar','June']" :values="[20,30,40,60]"></line-graph>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-xl-6 m-t-2">
      <div class="card">
        <div class="card-header">
          <h4><i class="fa fa-bar-chart text-success"></i> Yearly Sales</h4>
        </div>
        <div class="card-block">
          <bar-graph :labels="['Jan','Feb','Mar','June']" :values="[20,30,40,60]"></bar-graph>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xl-6 m-t-2">
      <div class="card">
        <div class="card-header">
          <h4><i class="fa fa-shopping-cart text-danger"></i> Recent Orders</h4>
        </div>
        <div class="card-block">
          <table class="table">
            <thead>
              <tr>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Walter White</td>
                <td>05/12/2016</td>
                <td>555$</td>
                <td><a href="#" class="btn btn-default btn-xs">View</a></td>
              </tr>
              <tr>
                <td>Hank Hill</td>
                <td>05/12/2016</td>
                <td>222$</td>
                <td><a href="#" class="btn btn-default btn-xs">View</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-xl-6 m-t-2">
      <div class="card">
        <div class="card-header">
          <h4><i class="fa fa-users text-info"></i> New Customers</h4>
        </div>
        <div class="card-block">
          <table class="table">
            <thead>
              <tr>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Walter White</td>
                <td>05/12/2016</td>
                <td>555$</td>
                <td><a href="#" class="btn btn-default btn-xs">View</a></td>
              </tr>
              <tr>
                <td>Hank Hill</td>
                <td>05/12/2016</td>
                <td>222$</td>
                <td><a href="#" class="btn btn-default btn-xs">View</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="site-footer">
    <div class="text-right">
      Login Register system
    </div>
</footer>
    <script src="assets/admin/js/core/plugins.js"></script>
    <script src="assets/admin/js/dashboard/dashboard.js"></script>
</body>
</html>
